package customer.main.model;

import lombok.Data;

@Data
public class Customer {

    private Integer id;

    private String firstname;

    private String lastname;

    private int age;
}
