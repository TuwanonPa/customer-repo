package customer.main.service;

import customer.main.repository.CustomerEntity;

import java.util.List;

public interface CustomerService {

    List<CustomerEntity> getAllCustomer();
    void saveCustomer(CustomerEntity customer);
}
