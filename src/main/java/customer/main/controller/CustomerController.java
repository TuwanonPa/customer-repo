package customer.main.controller;

import customer.main.repository.CustomerEntity;
import customer.main.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

;

@RestController
@RequestMapping(value = "/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/find")
    @ResponseBody
    public ResponseEntity<List<CustomerEntity>> getAllCustomer() {
        List<CustomerEntity> customerEntity = customerService.getAllCustomer();
        return new ResponseEntity<>(customerEntity, HttpStatus.OK);
    }

    @PostMapping("/insert")
    public CustomerEntity putCustomerEntity(@RequestBody CustomerEntity customerEntity){
//        CustomerEntity responseEntity = new CustomerEntity();
//        responseEntity.setLastname(customerEntity.getFirstname());
//        responseEntity.setLastname(customerEntity.getLastname());
//        responseEntity.setAge(customerEntity.getAge());
        customerService.saveCustomer(customerEntity);

        return customerEntity;
    }
}
